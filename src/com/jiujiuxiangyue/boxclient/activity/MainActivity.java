package com.jiujiuxiangyue.boxclient.activity;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.teleal.cling.android.AndroidUpnpService;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.UDAServiceType;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;
import org.teleal.cling.support.model.Res;
import org.teleal.cling.support.model.container.Container;
import org.teleal.common.logging.LoggingUtil;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.jiujiuxiangyue.boxclient.R;
import com.jiujiuxiangyue.boxclient.util.FixedAndroidHandler;

public class MainActivity extends Activity {

	private static final Logger log = Logger.getLogger(MainActivity.class
			.getName());

	private ListView deviceListView;
	private ListView contentListView;

	private ArrayAdapter<DeviceItem> deviceListAdapter;
	private ArrayAdapter<ContentItem> contentListAdapter;

	private AndroidUpnpService upnpService;

	private DeviceListRegistryListener deviceListRegistryListener;

	private static boolean serverPrepared = false;

	private final static String LOGTAG = "Boxclient";

	private ServiceConnection serviceConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName className, IBinder service) {
			upnpService = (AndroidUpnpService) service;
			Log.v(LOGTAG, "Connected to UPnP Service");

			deviceListAdapter.clear();
			for (Device device : upnpService.getRegistry().getDevices()) {
				deviceListRegistryListener.deviceAdded(new DeviceItem(device));
			}

			// Getting ready for future device advertisements
			upnpService.getRegistry().addListener(deviceListRegistryListener);
			// Refresh device list
			upnpService.getControlPoint().search();
		}

		public void onServiceDisconnected(ComponentName className) {
			upnpService = null;
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Fix the logging integration between java.util.logging and Android
		// internal logging
		LoggingUtil.resetRootHandler(new FixedAndroidHandler());
		Logger.getLogger("org.teleal.cling").setLevel(Level.INFO);

		/*
		 * Enable this for debug logging:
		 * 
		 * // UDP communication
		 * Logger.getLogger("org.teleal.cling.transport.spi.DatagramIO"
		 * ).setLevel(Level.FINE);
		 * Logger.getLogger("org.teleal.cling.transport.spi.MulticastReceiver"
		 * ).setLevel(Level.FINE);
		 * 
		 * // Discovery
		 * Logger.getLogger("org.teleal.cling.protocol.ProtocolFactory"
		 * ).setLevel(Level.FINER);
		 * Logger.getLogger("org.teleal.cling.protocol.async"
		 * ).setLevel(Level.FINER);
		 * 
		 * // Description
		 * Logger.getLogger("org.teleal.cling.protocol.ProtocolFactory"
		 * ).setLevel(Level.FINER);
		 * Logger.getLogger("org.teleal.cling.protocol.RetrieveRemoteDescriptors"
		 * ).setLevel(Level.FINE);
		 * Logger.getLogger("org.teleal.cling.transport.spi.StreamClient"
		 * ).setLevel(Level.FINEST);
		 * 
		 * Logger.getLogger("org.teleal.cling.protocol.sync.ReceivingRetrieval").
		 * setLevel(Level.FINE);
		 * Logger.getLogger("org.teleal.cling.binding.xml.DeviceDescriptorBinder"
		 * ).setLevel(Level.FINE);
		 * Logger.getLogger("org.teleal.cling.binding.xml.ServiceDescriptorBinder"
		 * ).setLevel(Level.FINE);
		 * Logger.getLogger("org.teleal.cling.transport.spi.SOAPActionProcessor"
		 * ).setLevel(Level.FINEST);
		 * 
		 * // Registry
		 * Logger.getLogger("org.teleal.cling.registry.Registry").setLevel
		 * (Level.FINER);
		 * Logger.getLogger("org.teleal.cling.registry.LocalItems"
		 * ).setLevel(Level.FINER);
		 * Logger.getLogger("org.teleal.cling.registry.RemoteItems"
		 * ).setLevel(Level.FINER);
		 */

		setContentView(R.layout.main);

		deviceListView = (ListView) findViewById(R.id.deviceList);
		contentListView = (ListView) findViewById(R.id.contentList);

		deviceListAdapter = new ArrayAdapter<DeviceItem>(this,
				android.R.layout.simple_list_item_1);
		deviceListRegistryListener = new DeviceListRegistryListener();
		deviceListView.setAdapter(deviceListAdapter);
		deviceListView.setOnItemClickListener(deviceItemClickListener);

		contentListAdapter = new ArrayAdapter<ContentItem>(this,
				android.R.layout.simple_list_item_1);
		contentListView.setAdapter(contentListAdapter);
		contentListView.setOnItemClickListener(contentItemClickListener);

		getApplicationContext().bindService(
				new Intent(this, WireUpnpService.class), serviceConnection,
				Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (upnpService != null) {
			upnpService.getRegistry()
					.removeListener(deviceListRegistryListener);
		}
		getApplicationContext().unbindService(serviceConnection);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 0, 0, R.string.search_lan).setIcon(
				android.R.drawable.ic_menu_search);
		menu.add(0, 1, 0, R.string.toggle_debug_logging).setIcon(
				android.R.drawable.ic_menu_info_details);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 0:
			searchNetwork();
			break;
		case 1:
			Logger logger = Logger.getLogger("org.teleal.cling");
			if (logger.getLevel().equals(Level.FINEST)) {
				Toast.makeText(this, R.string.disabling_debug_logging,
						Toast.LENGTH_SHORT).show();
				logger.setLevel(Level.INFO);
			} else {
				Toast.makeText(this, R.string.enabling_debug_logging,
						Toast.LENGTH_SHORT).show();
				logger.setLevel(Level.FINEST);
			}
			break;
		}
		return false;
	}

	protected void searchNetwork() {
		if (upnpService == null)
			return;
		Toast.makeText(this, R.string.searching_lan, Toast.LENGTH_SHORT).show();
		upnpService.getRegistry().removeAllRemoteDevices();
		upnpService.getControlPoint().search();
	}

	OnItemClickListener deviceItemClickListener = new OnItemClickListener() {

		protected Container createRootContainer(Service service) {
			Container rootContainer = new Container();
			rootContainer.setId("0");
			rootContainer.setTitle("Content Directory on "
					+ service.getDevice().getDisplayString());
			return rootContainer;
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position,
				long id) {
			// TODO Auto-generated method stub
			Device device = deviceListAdapter.getItem(position).getDevice();
			Service service = device.findService(new UDAServiceType(
					"ContentDirectory"));
			upnpService.getControlPoint().execute(
					new ContentBrowseActionCallback(MainActivity.this, service,
							createRootContainer(service), contentListAdapter));
		}
	};

	OnItemClickListener contentItemClickListener = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View v, int position,
				long id) {
			// TODO Auto-generated method stub
			ContentItem content = contentListAdapter.getItem(position);
			if (content.isContainer()) {
				upnpService.getControlPoint().execute(
						new ContentBrowseActionCallback(MainActivity.this,
								content.getService(), content.getContainer(),
								contentListAdapter));
			} else {
				Intent intent = new Intent();
				Res res = content.getItem().getFirstResource();
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.setAction(Intent.ACTION_VIEW);
				intent.setDataAndType(Uri.parse(res.getValue()), res
						.getProtocolInfo().getContentFormat());
				startActivity(intent);
			}
		}
	};

	public class DeviceListRegistryListener extends DefaultRegistryListener {

		/* Discovery performance optimization for very slow Android devices! */

		@Override
		public void remoteDeviceDiscoveryStarted(Registry registry,
				RemoteDevice device) {
		}

		@Override
		public void remoteDeviceDiscoveryFailed(Registry registry,
				final RemoteDevice device, final Exception ex) {
		}

		/*
		 * End of optimization, you can remove the whole block if your Android
		 * handset is fast (>= 600 Mhz)
		 */

		@Override
		public void remoteDeviceAdded(Registry registry, RemoteDevice device) {

			if (device.getType().getNamespace().equals("schemas-upnp-org")
					&& device.getType().getType().equals("MediaServer")) {
				final DeviceItem display = new DeviceItem(device, device
						.getDetails().getFriendlyName(),
						device.getDisplayString(), "(REMOTE) "
								+ device.getType().getDisplayString());
				deviceAdded(display);
			}
		}

		@Override
		public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
			final DeviceItem display = new DeviceItem(device,
					device.getDisplayString());
			deviceRemoved(display);
		}

		@Override
		public void localDeviceAdded(Registry registry, LocalDevice device) {
			final DeviceItem display = new DeviceItem(device, device
					.getDetails().getFriendlyName(), device.getDisplayString(),
					"(REMOTE) " + device.getType().getDisplayString());
			deviceAdded(display);
		}

		@Override
		public void localDeviceRemoved(Registry registry, LocalDevice device) {
			final DeviceItem display = new DeviceItem(device,
					device.getDisplayString());
			deviceRemoved(display);
		}

		public void deviceAdded(final DeviceItem di) {
			runOnUiThread(new Runnable() {
				public void run() {

					int position = deviceListAdapter.getPosition(di);
					if (position >= 0) {
						// Device already in the list, re-set new value at same
						// position
						deviceListAdapter.remove(di);
						deviceListAdapter.insert(di, position);
					} else {
						deviceListAdapter.add(di);
					}

					// Sort it?
					// listAdapter.sort(DISPLAY_COMPARATOR);
					// listAdapter.notifyDataSetChanged();
				}
			});
		}

		public void deviceRemoved(final DeviceItem di) {
			runOnUiThread(new Runnable() {
				public void run() {
					deviceListAdapter.remove(di);
				}
			});
		}
	}

	// FIXME: now only can get wifi address
	private InetAddress getLocalIpAddress() throws UnknownHostException {
		WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		int ipAddress = wifiInfo.getIpAddress();
		return InetAddress.getByName(String.format("%d.%d.%d.%d",
				(ipAddress & 0xff), (ipAddress >> 8 & 0xff),
				(ipAddress >> 16 & 0xff), (ipAddress >> 24 & 0xff)));
	}
}